//
//  AppDelegate.swift
//  appkit_arena
//
//  Created by slbtty on 2022-04-01.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet var window: NSWindow!

    @objc func buttonTest() {
        print("Test button")
    }

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        let button = NSButton(title: "A button in code", target: self, action: #selector(buttonTest))
        window!.contentView?.addSubview(button)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }


}

